# DEMM lessons


## Software Prerequisites 

1. Python>=3 
2. PIP
3. GIT 
4. DOCKER (https://docs.docker.com/docker-for-windows/install/)
5. IDE (e.g. Visual Studio Code/PyCharm)


## Slides

1. https://docs.google.com/presentation/d/e/2PACX-1vQ7T9cUshpPI_2fepjhAmTCgwOkB2DaJQGVhS3ZF1dBzqODlZKQ8Na9_05gK3VwDqmaWtbEB2Dm5Jxm/pub?start=false&loop=false&delayms=3000

2. https://docs.google.com/presentation/d/e/2PACX-1vQNVIjJmGBh8-335mxpaJcE6TSjacMs6kNSHMxElNNn6Xi4cLCU4SbVV6miKrWOnQOCQnaF-0wRw2Wx/pub?start=false&loop=false&delayms=60000
